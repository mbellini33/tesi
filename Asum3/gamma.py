##GAMMA PARAMETER CALCULATION

import nltk
#nltk.download('punkt')
import pandas as pd
from nltk.tokenize import sent_tokenize
from Preprocessing.PreProcFunc import PreRemove,removeStopwordsPunct
from nltk.sentiment.vader import SentimentIntensityAnalyzer
import numpy as np
from statistics import mean

Phrase = pd.read_csv('/Users/mbellini/Google Drive/Tesi/Progetto/Preprocessing/2.PrefilteredText/PreFilterTextsEnd.xls')

Phrase = Phrase.drop(['Unnamed: 0','Link','Match','Language'],1)

#Preprocessing for Phrases
Phrase['Text']= Phrase['Text'].astype(str).apply(lambda row: sent_tokenize(row))
#Add zeros Column
Phrase['Score'] = pd.Series(np.zeros(len(Phrase)))


####Ho modificato una riga e poi sostituita come lista: se calcolo
#la polarità per ciascuna riga e riconverto posso automatizzare tutto senza
#impazzire nel calcolo delle singole righe
sid = SentimentIntensityAnalyzer()
ss = sid.polarity_scores
a = len(Phrase)

means = list()

for i in range(0,2):
 df =pd.Series(Phrase['Text'].loc[i])
 df =df.astype(str).apply(lambda row: PreRemove(row))
 df =df.astype(str).apply(lambda row: removeStopwordsPunct(row))
 df= pd.DataFrame(df)
 df['Score'] = pd.Series(np.zeros(len(df)))
 #df['Score'] = df[0].astype(str).apply(lambda row: sid.polarity_scores(row))
 df['Score'] = df[0].astype(str).map(sid.polarity_scores)
 df['Score'] = df['Score'].apply(lambda row:row['compound'])
 df = df['Score'].tolist()
 res = mean(df) 
 means.append(res)


Phrase['Score'] = means
#Substitute Pos And Neg
Phrase['Score'] = Phrase['Score'].mask(Phrase.Score > 0 , 1)
Phrase['Score'] = Phrase['Score'].mask(Phrase.Score < 0 , -1)

#Number of elements Neg Pos And Neutral
counts = (Phrase.groupby('Score').count()) / a



