####################Asum Entrance#################
import pandas as pd
from collections import Counter
from nltk.tokenize import sent_tokenize
import re
from Preprocessing.PreProcessing2 import removeStopwordsPunct,PorterStemm

#################################################1.Bag Of Words#############################

LanguageResult = pd.read_csv('/Users/mbellini/Google Drive/Tesi/Progetto/Preprocessing/2.PrefilteredText/Stemming.xls')
LanguageResult = LanguageResult.drop(['Unnamed: 0','Unnamed: 0.1','Link','Match','Language'],1)

#Bag of word as Dataframe
LanguageResult['Text']= LanguageResult['Text'].astype(str).apply(lambda row: row.split())
Length = len(LanguageResult)

#Bag Of Word List Divisions
Words = LanguageResult['Text'].tolist()
r = len(Words)
hh = list()

for i in Words[0:Length]:
    for j in i:
        hh.append(j)

#Elimino gli elementi di lunghezza 1
hh = [i for i in hh if len(i) > 1]
#Statistica per la Bag Of Words
c = Counter(hh).most_common(5000)


prov = list()
for line in c:
    prov.append(line[0])


#############
#Create Word Of List File
def CreateOut(file,name):
 file = open('C:/Users/Administrator/Desktop/' + name + '.txt','w',encoding='utf-8')
 for line in c:
   file.write(line[0])
   file.write("\n")
 file.close()
 return


########################################################################################################################
#2.Bag Of Sentence
Phrase = pd.read_csv('C:/Users/Administrator/Desktop/PrefilteredText/PreFilterTextsEnd.xls')

Phrase = Phrase.drop(['Unnamed: 0','Link','Match','Language'],1)

#Preprocessing for Phrases


Phrase['Text']= Phrase['Text'].astype(str).apply(lambda row: sent_tokenize(row))

############## Numero di conteggi tra la wordlist e la bag of sentence
t = r//2

Prova = Phrase[0:t]

#Sentences = Phrase['Text'].tolist()

Sentences = Prova['Text'].tolist()

r = len(Sentences)

##Ok#

def BagofSentence(Sentences):
 for i in Sentences:
  print(Sentences.index(i))
  Occurences = list()
  Occurences.append(len(i))
  sent = list()
 for h in i:
  sent.append(h)
  sent = [removeStopwordsPunct(row) for row in sent]
  sent = [PorterStemm(row) for row in sent]
 for j in sent:
    ll = j.split()
    en = [([n for n, item2 in enumerate(prov) if item1 == item2]) for item1 in ll]
    Occurences.append(en)
 return


###############################
def openfile(list,name):
 bow2 = list()
 with open('C:/Users/Administrator/Desktop/Asum/FileIntermediEBackUp/BagOfSentence2.csv','r',encoding='utf-8') as input:
    for s in input:
       bow2.append(s.strip())
 return

def cleantest(text):
 bow2 = [i.replace('[','').replace(']','').replace(',','').replace('  ',' ') for i in bow2]
 bow2 = [re.sub(' +',' ',i) for i in bow2]
 bow2 = [re.sub(r'^ ','',i) for i in bow2]
 return text



def create_file(file,name):
 with open('C:/Users/Administrator/Desktop/' + name + '.txt','w',encoding='utf-8') as file:
    for i in file:
       print(file.index(i))
       file.append(i.strip())
 return



 with open('C:/Users/Administrator/Desktop/BagofSentence2.txt','w',encoding='utf-8') as file:
    for i in bow2:
       print(bow2.index(i))
       file.write(i)
       file.write("\n")
    file.close()

###############Creation of sentiment words vocabulary################
from nltk.sentiment.vader import SentimentIntensityAnalyzer
sid = SentimentIntensityAnalyzer()
ss = sid.polarity_scores

#Biuld my own dictionary
file = pd.read_csv('/Users/mbellini/Desktop/Asumfiles/wordlist.txt',header=None)
file['Score'] = file[0].astype(str).map(sid.polarity_scores)
file['Score'] = file['Score'].apply(lambda row: row['compound'])
file['Score'] = file['Score'].mask(file.Score > 0 , 1)
file['Score'] = file['Score'].mask(file.Score < 0 , -1)
###File Construction
#0 file
zero= file[0].loc[file['Score'] > 0]
#1 file
one= file[0].loc[file['Score'] == 0]
#2 file
two= file[0].loc[file['Score'] < 0]

#Write files to csv vedere al lavoro
zero.to_csv(r'/Users/mbellini/Desktop/Asumfiles/SentiWords-0.txt', header=None, index=None, sep=' ', mode='a')
one.to_csv(r'/Users/mbellini/Desktop/Asumfiles/SentiWords-1.txt', header=None, index=None, sep=' ', mode='a')
two.to_csv(r'/Users/mbellini/Desktop/Asumfiles/SentiWords-2.txt', header=None, index=None, sep=' ', mode='a')