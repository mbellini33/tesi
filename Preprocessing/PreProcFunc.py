#Pre Processing
import pandas as pd
#from nltk.corpus import stopwords
import re
from nltk.stem.porter import PorterStemmer

stopwords = list()
with open('/Users/mbellini/Google Drive/Tesi/Progetto/Preprocessing/2.PrefilteredText/stopwords.csv','r',encoding='utf-8') as input:
    for line in input:
       stopwords.append(line.strip())

def PreRemove(s):
    specialwords = ['us']
    word_list = s.split()
    filtered_words = [word for word in word_list if word not in specialwords]
    s = ' '.join(filtered_words)
    return s

def removeStopwordsPunct(s):
    s=s.lower()
    #Tokenization
    word_list = s.split()
    #StopWords Better
    filtered_words = [word for word in word_list if word not in stopwords]
    filt = ' '.join(filtered_words)

    # Rimuovo i siti internet,ovvero gli url completi e interi
    s = re.sub(r'\w+:\/{2}[\d\w-]+(\.[\d\w-]+)*(?:(?:\/[^\s/]*))*', '', filt)

    #remove puntuaction and strange characters keeping u.s word
     #After word U.K U.S
    s = re.sub(r'[^\w\s](?!s)',' ',s)
    s = re.sub(r'[^\w\s](?!k)', ' ', s)
     #Before Words
    s = re.sub(r'(?<!u)[^\w\s]',' ',s)
    #remove numbers with st,sd,rd,th etc
    s = re.sub(r'\d{1,2}[a-zA-z]{2}\b','',s)
    # Substitute u.s with us
    s = re.sub(r'\bu.s\b', 'us', s)
    s = re.sub(r'\bu.k\b', 'uk', s)
    #remove digits
    final = re.sub(r'\d+','',s)
    return final.lower()
