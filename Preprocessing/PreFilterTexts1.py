##1.

################### Filtrare i testi che hanno contenuto completamente inutile per fine di analisi
import pandas as pd
import re
from langdetect import detect
#Read Text Files

df1 = pd.read_csv('/Users/mbellini/Google Drive/Tesi/Progetto/Preprocessing/Articles/1.xls')
df2 = pd.read_csv('/Users/mbellini/Google Drive/Tesi/Progetto/Preprocessing/Articles/2.xls')
df3 = pd.read_csv('/Users/mbellini/Google Drive/Tesi/Progetto/Preprocessing/Articles/3.xls')

#Unifying
frames = [df1, df2, df3]
result = pd.concat(frames)


###########################Filter Rules###################################################################
#Filter Out Nan Text
result = result[result['Text'].notnull()]

result = result.drop(['Unnamed: 0'],1)
b = len(result)
index = list(range(0,b))
result['Index'] = index


#Language Detection
text = result['Text'].values.tolist()

ris=[]
ris.append([])
ris.append([])

for t in text:
    ris[0].append(t)
    try:
      a = detect(t)
    except:
      a = 0
    ris[1].append(a)


Language = pd.DataFrame({'Text': ris[0],'Language' : ris[1]})

Language['Index'] = index

#Join tra le due tabelle + puliza colonne
LanguageResult = pd.merge(result,Language,on=['Index'])
LanguageResult = LanguageResult.drop(['Text_y'],1)
LanguageResult = LanguageResult.drop(['Index'],1)
LanguageResult = LanguageResult[LanguageResult['Language'].isin(['en',0])]

#Most important words talked in Brexit##########################################################################
###parole chiave da inserire nel match
LanguageResult = LanguageResult.rename(index=str, columns={"Text_x": "Text"})
LanguageResult['Match'] = LanguageResult['Text'].apply(lambda row: re.search(r'\bbrexit\b|\bBrexit\b|\bMay\b|\bEU\b|\bJohnson\b|\bTory\b|\bFarage\b|\bCorbyn\b|\bSturgeon\b|\bIndependence\b|\bindependence\b|\beu\b',row))
LanguageResult = LanguageResult[LanguageResult['Match'].notnull()]

#######################################

print(LanguageResult.to_csv('C:/Users/Administrator/Desktop/PreFilterTextsEnd.xls',encoding='utf-8',index=True, header=True))

