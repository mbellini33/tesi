# Mi collego al database Neo4j

from neo4jrestclient.client import GraphDatabase
from neo4jrestclient import client

import pandas as pd
db = GraphDatabase("http://localhost:7474", username="neo4j", password="mindbrexit")

q = 'MATCH(anything:News) RETURN anything'

results = db.query(q,returns=(client.Node))

#Mi stampa tutti i links da utilizzare nel bot

#inizializzo la lista
a = list()
b = list()
c = list()

#Lista Nodi
a = [a.append(r[0])for r in results]
#Lista Links
b = [b.append(s["url"]) for s in a]
#creo una sottolista per esportare il file e rendere leggibile di scrapy i links
i=len(b)
for t in b[0:i]:
    c.append(t)

#SCrivere un csv mi porta a un accelerazione del processo
df = pd.DataFrame(c)
print(df.to_csv('C:/Users/mi.bellini/Desktop/tutorial/Dati/entry.csv', encoding='utf-8', index=False,
                    header=False, sep='\n'))

#Filtrazione dei links che fanno morire il bot
#Sono della forma http%www. e https% ovvero il simbolo % dopo l'http provoca questo effetto