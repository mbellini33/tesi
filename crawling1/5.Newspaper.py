#Tutti i links con risultato pari a 200 filtrati dallo script precedente

from newspaper import Article
import pandas as pd
import time
import csv


def openfile(string):
 urls = list()
 global urls
 with open (string) as input:
     for line in input:
         urls.append(line.strip())
 return

def Articledownload(urls,pathfile):
 text=[]
 text.append([])
 text.append([])
 #url=''
 for url in urls:
     a = Article(url)
     a.download()
     time.sleep(0.5)
     try:
      a.parse()
     except :
      pass
     else:
         text[0].append(a.text)
         text[1].append(url)
 a = text[0]
 b= text[1]
 export_list = pd.DataFrame(
     {'Text' : a,
      'Link' : b,
      })
 print(export_list.to_excel(pathfile,encoding='utf-8',index=True, header=True))
 return


# Urls =  C:/Users/Administrator/Desktop/tuttilink.csv'
#print(export_list.to_csv('C:/Users/Administrator/Desktop/20mila.xls',encoding='utf-8',index=True, header=True))

